import { Player } from './Player';

class HumanPlayer extends Player {
	constructor(name: string, symbol: 'X' | 'O') {
		super(name, symbol);
	}

	public makeMove(row: number, col: number): void {
		console.log(`Move made at row ${row}, col ${col} by ${this.name}`);

		const cellSelector = `.cell[data-row="${row}"][data-col="${col}"]`;
		const cell = document.querySelector(cellSelector);
		if (cell && cell.textContent === '') {
			cell.textContent = this.symbol;
		}
	}
}

export { HumanPlayer };
