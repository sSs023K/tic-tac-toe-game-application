interface LeaderBoardEntry {
	name: string;
	gamesPlayed: number;
	wins: number;
}

class LeaderBoard {
	private entries: LeaderBoardEntry[];

	constructor() {
		this.entries = [];
		this.render();
	}

	public addEntry(name: string, gamesPlayed: number, wins: number): void {
		const existingEntry = this.entries.find((entry) => entry.name === name);
		if (existingEntry) {
			existingEntry.gamesPlayed += gamesPlayed;
			existingEntry.wins += wins;
		} else {
			this.entries.push({ name, gamesPlayed, wins });
		}
		this.render();
	}

	public render(): void {
		const boardElement = document.getElementById('leader-board');
		if (boardElement) {
			boardElement.innerHTML = `
                <table>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Games Played</th>
                            <th>Wins</th>
                        </tr>
                    </thead>
                    <tbody>
                        ${this.entries
													.map(
														(entry) => `
                                <tr>
                                    <td>${entry.name}</td>
                                    <td>${entry.gamesPlayed}</td>
                                    <td>${entry.wins}</td>
                                </tr>
                            `
													)
													.join('')}
                    </tbody>
                </table>
            `;
		}
	}
}

export { LeaderBoard };
export type { LeaderBoardEntry };
