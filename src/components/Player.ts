abstract class Player {
	name: string;
	symbol: 'X' | 'O';

	constructor(name: string, symbol: 'X' | 'O') {
		this.name = name;
		this.symbol = symbol;
	}

	abstract makeMove(row: number, col: number): void;
}

export { Player };
