import { HumanPlayer } from '../HumanPlayer';

function testMakeMove() {
  console.log('Testing makeMove...');
  
  // Mocking HTML structure
  document.body.innerHTML = `
    <div class="cell" data-row="0" data-col="0"></div>
    <div class="cell" data-row="0" data-col="1"></div>
    <div class="cell" data-row="0" data-col="2"></div>
    <div class="cell" data-row="1" data-col="0"></div>
    <div class="cell" data-row="1" data-col="1"></div>
    <div class="cell" data-row="1" data-col="2"></div>
    <div class="cell" data-row="2" data-col="0"></div>
    <div class="cell" data-row="2" data-col="1"></div>
    <div class="cell" data-row="2" data-col="2"></div>
  `;
  
  const player = new HumanPlayer('John', 'X');
  
  // Mocking click event on a cell
  const cell = document.querySelector('.cell[data-row="0"][data-col="0"]');
  cell?.dispatchEvent(new MouseEvent('click'));

  // Checking if the move was made correctly
  const movedCell = document.querySelector('.cell[data-row="0"][data-col="0"]');
  if (movedCell && movedCell.textContent === 'X') {
    console.log('Move made successfully!');
  } else {
    console.error('Failed to make move!');
  }
}

function runTests() {
  testMakeMove();
}

runTests();
