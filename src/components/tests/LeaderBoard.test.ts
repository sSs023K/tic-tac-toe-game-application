import { LeaderBoard } from '../LeaderBoard';

function testAddEntry() {
  console.log('Testing addEntry...');
  
  const leaderBoard = new LeaderBoard();
  
  // Adding a new entry
  leaderBoard.addEntry('John', 5, 3);
  
  // Checking if the entry was added correctly
  const entries = leaderBoard['entries'];
  const newEntry = entries.find(entry => entry.name === 'John');
  if (newEntry && newEntry.gamesPlayed === 5 && newEntry.wins === 3) {
    console.log('Entry added successfully!');
  } else {
    console.error('Failed to add entry!');
  }
}

function runTests() {
  testAddEntry();
}

runTests();
