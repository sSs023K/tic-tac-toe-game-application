import { Player } from '../Player';

class MockPlayer extends Player {
  constructor(name: string, symbol: 'X' | 'O') {
    super(name, symbol);
  }

  makeMove(row: number, col: number): void {
    console.log(`${this.name} makes a move at row ${row}, col ${col}`);
    // Here you can add any specific logic for testing the makeMove method
  }
}

function testMakeMove() {
  console.log('Testing makeMove...');
  
  const player = new MockPlayer('John', 'X');
  player.makeMove(0, 0);
  // Add more assertions if needed
}

function runTests() {
  testMakeMove();
  // Add more test functions if needed
}

runTests();
