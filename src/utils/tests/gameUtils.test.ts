import { initiateBoard, verifyWin, verifyDraw } from '../gameUtils';
import { ICell } from '../../interfaces/GameInterface';

function testInitiateBoard() {
  console.log('Testing initiateBoard...');
  const board = initiateBoard();
  console.log('Board:', board);
  // Add more assertions if needed
}

function testVerifyWin() {
  console.log('Testing verifyWin...');
  const board: ICell[][] = [
    [{ row: 0, col: 0, value: 'X' }, { row: 0, col: 1, value: 'X' }, { row: 0, col: 2, value: 'X' }],
    [{ row: 1, col: 0, value: null }, { row: 1, col: 1, value: null }, { row: 1, col: 2, value: null }],
    [{ row: 2, col: 0, value: null }, { row: 2, col: 1, value: null }, { row: 2, col: 2, value: null }]
  ];
  console.log('Board:', board);
  console.log('Win:', verifyWin(board)); // Should return true for this board
}

function testVerifyDraw() {
  console.log('Testing verifyDraw...');
  const board: ICell[][] = [
    [{ row: 0, col: 0, value: 'X' }, { row: 0, col: 1, value: 'O' }, { row: 0, col: 2, value: 'X' }],
    [{ row: 1, col: 0, value: 'X' }, { row: 1, col: 1, value: 'O' }, { row: 1, col: 2, value: 'O' }],
    [{ row: 2, col: 0, value: 'O' }, { row: 2, col: 1, value: 'X' }, { row: 2, col: 2, value: 'X' }]
  ];
  console.log('Board:', board);
  console.log('Draw:', verifyDraw(board)); // Should return true for this board
}

function runTests() {
  testInitiateBoard();
  testVerifyWin();
  testVerifyDraw();
}

runTests();
