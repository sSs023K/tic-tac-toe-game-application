import { HumanPlayer } from './components/HumanPlayer';
import { GameState } from './enums/GameState';
import { initiateBoard, verifyDraw, verifyWin } from './utils/gameUtils';
import { ICell } from './interfaces/GameInterface';
import { LeaderBoard } from './components/LeaderBoard';

class TicTacToeGame {
	private grid: ICell[][];
	private activePlayer: HumanPlayer;
	private player1: HumanPlayer;
	private player2: HumanPlayer;
	private currentGameState: GameState;
	private scoreBoard: LeaderBoard;

	constructor() {
		this.grid = initiateBoard();
		this.player1 = new HumanPlayer('Player 1', 'X');
		this.player2 = new HumanPlayer('Player 2', 'O');
		this.activePlayer = this.player1;
		this.currentGameState = GameState.CONTINUE;
		this.scoreBoard = new LeaderBoard();
		this.setupEventListeners();
		this.renderBoard();
		this.updateStatusDisplay(`${this.activePlayer.name}'s turn`);
	}

	private setupEventListeners(): void {
		const gridCells = document.querySelectorAll('.cell');
		gridCells.forEach((cell) => {
			cell.addEventListener('click', (event) => this.handleCellClick(event));
		});
		const restartButton = document.getElementById('restart-button');
		restartButton?.addEventListener('click', () => this.resetGame());

		const popupBtn = document.getElementById('rulesBtn')!;
		const closeBtn = document.getElementById('closePopup')!;
		const popup = document.getElementById('popup')!;
		const overlay = document.getElementById('overlay')!;

		function hideRules() {
			overlay.classList.remove('show');
			popup.classList.remove('show');
		}

		overlay.addEventListener('click', hideRules);
		closeBtn.addEventListener('click', hideRules);
		popupBtn.addEventListener('click', function () {
			popup.classList.add('show');
			overlay.classList.add('show');
		});
	}

	private handleCellClick(event: Event): void {
		const selectedCell = event.target as HTMLElement;
		const row = parseInt(selectedCell.dataset.row!);
		const col = parseInt(selectedCell.dataset.col!);
		console.log(this.currentGameState === GameState.CONTINUE);
		if (
			this.grid[row][col].value ||
			this.currentGameState !== GameState.CONTINUE
		) {
			return;
		}
		this.grid[row][col].value = this.activePlayer.symbol;
		selectedCell.textContent = this.activePlayer.symbol;

		if (verifyWin(this.grid)) {
			this.currentGameState =
				this.activePlayer.symbol === 'X' ? GameState.X_WINS : GameState.O_WINS;
			this.updateStatusDisplay(`${this.activePlayer.name} wins!`);
			this.scoreBoard.addEntry(this.activePlayer.name, 1, 1);
		} else if (verifyDraw(this.grid)) {
			this.currentGameState = GameState.DRAW;
			this.updateStatusDisplay("It's a draw!");
		} else {
			this.switchPlayer();
		}
	}

	private switchPlayer(): void {
		this.activePlayer =
			this.activePlayer === this.player1 ? this.player2 : this.player1;
		this.updateStatusDisplay(`${this.activePlayer.name}'s turn`);
	}

	private updateStatusDisplay(message: string): void {
		const displayElement = document.getElementById('status');
		if (displayElement) {
			displayElement.textContent = message;
		}
	}

	private resetGame(): void {
		this.grid = initiateBoard();
		this.currentGameState = GameState.CONTINUE;
		this.activePlayer = this.player1;
		this.renderBoard();
		this.updateStatusDisplay("Player 1's turn");
	}

	private renderBoard(): void {
		this.grid.forEach((row, rowIndex) => {
			row.forEach((cell, colIndex) => {
				const cellElement = document.querySelector(
					`.cell[data-row="${rowIndex}"][data-col="${colIndex}"]`
				);
				if (cellElement) {
					cellElement.textContent = cell.value;
				}
			});
		});
		const leaderBoardEl = document.getElementById('leader-board');

		if (leaderBoardEl) {
			leaderBoardEl.innerHTML = '';
			this.scoreBoard.render();
		}
	}
}

function main() {
	document.addEventListener('DOMContentLoaded', () => {
		new TicTacToeGame();
	});
}

main();
export default TicTacToeGame;
