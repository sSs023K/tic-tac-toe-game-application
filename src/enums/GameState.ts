class GameState {
	static readonly CONTINUE = 'CONTINUE';
	static readonly X_WINS = 'X_WINS';
	static readonly O_WINS = 'O_WINS';
	static readonly DRAW = 'DRAW';
	static readonly STOPPED = 'STOPPED';
}

export { GameState };
