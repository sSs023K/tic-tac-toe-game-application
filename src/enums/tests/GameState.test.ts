class GameState {
	static readonly CONTINUE = 'CONTINUE';
	static readonly X_WINS = 'X_WINS';
	static readonly O_WINS = 'O_WINS';
	static readonly DRAW = 'DRAW';
	static readonly STOPPED = 'STOPPED';
  
	static test() {
	  console.log('Testing GameState...');
	  console.log('CONTINUE:', GameState.CONTINUE);
	  console.log('X_WINS:', GameState.X_WINS);
	  console.log('O_WINS:', GameState.O_WINS);
	  console.log('DRAW:', GameState.DRAW);
	  console.log('STOPPED:', GameState.STOPPED);
	}
  }
  
  GameState.test();
  