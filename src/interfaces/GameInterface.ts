interface IPlayer {
	name: string;
	symbol: 'X' | 'O'; // Player's symbol
	makeMove: (board: ICell[][]) => void; // Function to make a move on the board
}

interface ICell {
	row: number;
	col: number;
	value: 'X' | 'O' | null; // Cell can be empty (null), or filled with 'X' or 'O'
}

interface IGameBoard {
	cells: ICell[][]; // Two-dimensional array of cells
	verifyWin: () => boolean; // Function to verify if there's a winning condition
	verifyDraw: () => boolean; // Function to verify if the game is a draw
}

interface IGameState {
	currentPlayer: IPlayer;
	board: IGameBoard;
	gameState: 'CONTINUE' | 'X_WINS' | 'O_WINS' | 'DRAW' | 'STOPPED';
	changePlayer: () => void; // Function to switch the current player
	resetGame: () => void; // Function to reset the game to its initial state
}

export type { IPlayer, ICell, IGameBoard, IGameState };
